import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateTasksTable1643812275447 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: "tasks",
                columns: [
                    {
                        name: "id",
                        type: "uuid",
                        isPrimary: true,
                        generationStrategy: "uuid",
                        default: "uuid_generate_v4()",
                    },
                    {
                        name: "userId",
                        type: "uuid",
                        isNullable: false,
                    },
                    {
                        name: "description",
                        type: "varchar",
                    },
                    {
                        name: "status",
                        type: "enum",
                        isNullable: false,
                        enum: ["pending", "completed"],
                        enumName: "task_status",
                        default: "'pending'",
                    },
                    {
                        name: "createdOn",
                        type: "timestamp",
                        default: "now()",
                    },
                    {
                        name: "updatedOn",
                        type: "timestamp",
                        default: "now()",
                    }
                ]
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("tasks");
    }

}
