import {MigrationInterface, QueryRunner, TableForeignKey} from "typeorm";

export class AddReferencesOnTasks1643812856511 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createForeignKey(
            "tasks",
            new TableForeignKey({
                name: "UsersFK",
                columnNames: ["userId"],
                referencedColumnNames: ["id"],
                referencedTableName: "users",
                onDelete: "CASCADE",
                onUpdate: "CASCADE",
            })
        ); 
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey("tasks", "UsersFK");
    }

}