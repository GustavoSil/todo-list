import {MigrationInterface, QueryRunner, TableForeignKey} from "typeorm";

export class AddReferencesOnOrdenedTasks1644509745543 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createForeignKey(
            "ordened_tasks",
            new TableForeignKey({
                name: "UsersFK",
                columnNames: ["userId"],
                referencedColumnNames: ["id"],
                referencedTableName: "users",
                onDelete: "CASCADE",
                onUpdate: "CASCADE",
            })
        ); 

        await queryRunner.createForeignKey(
            "ordened_tasks",
            new TableForeignKey({
                name: "TasksFK",
                columnNames: ["taskId"],
                referencedColumnNames: ["id"],
                referencedTableName: "tasks",
                onDelete: "CASCADE",
                onUpdate: "CASCADE",
            })
        ); 
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey("ordened_tasks", "UsersFK");
        await queryRunner.dropForeignKey("ordened_tasks", "TasksFK");
    }

}
