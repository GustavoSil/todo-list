import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateOrdenedTasksTable1644509598480 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: "ordened_tasks",
                columns: [
                    {
                        name: "id",
                        type: "uuid",
                        isPrimary: true,
                        generationStrategy: "uuid",
                        default: "uuid_generate_v4()",
                    },
                    {
                        name: "userId",
                        type: "uuid",
                        isNullable: false,
                    },
                    {
                        name: "taskId",
                        type: "uuid",
                        isNullable: false,
                        isUnique: true,
                    },
                    {
                        name: "order",
                        type: "int",
                        isUnique: true,
                    },
                    {
                        name: "createdOn",
                        type: "timestamp",
                        default: "now()",
                    },
                    {
                        name: "updatedOn",
                        type: "timestamp",
                        default: "now()",
                    }
                ]
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("ordened_tasks");
    }

}
