import * as yup from "yup";

export const createOrdenedTasksSchema = yup.object().shape({
    ordenedTasks: yup.array().of(
        yup.object().shape({
            taskId: yup.string().required(),
            order: yup.number().required()
        })
    ).required()
});