import * as yup from "yup";

export const createTaskSchema = yup.object().shape({
    description: yup.string().required()
});
