import * as yup from "yup";

export const updateTaskSchema = yup.object().shape({
    description: yup.string(),
    status: yup.string().matches(/pending|completed/)
});
