import { Request, Response } from "express";
import AuthService from "../../services/Session/AuthService";

export const login = async (request: Request, response: Response) => {
    const { email, password } = request.body;

    const authUser = new AuthService();
    const tokenResponse = await authUser.execute({
        email,
        password,
    });

    return response.json(tokenResponse);
}
