import { instanceToInstance } from "class-transformer";
import { Request, Response } from "express";
import CreateTaskService from "../../services/Task/CreateTaskService";

export const create = async (request: Request, response: Response) => {
    const { description } = request.body;

    const createTaskService = new CreateTaskService();
    const task = await createTaskService.execute({
        description,
        userId: request.user.id,
    });
  
    return response.status(201).json(instanceToInstance(task));
};
