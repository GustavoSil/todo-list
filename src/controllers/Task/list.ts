import { Request, Response } from "express";
import ListTasksService from "../../services/Task/ListTasksService";

export const list = async (request: Request, response: Response) => {
    const listTaskService = new ListTasksService();
    const tasks = await listTaskService.execute({
        userId: request.user.id,
        params: request.query,
    });
  
    return response.json(tasks);
};
