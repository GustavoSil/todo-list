import { Request, Response } from "express";
import UpdateTasksService from "../../services/Task/UpdateTaskService";

export const update = async (request: Request, response: Response) => {
  const { id } = request.params;

  const updateTaskService = new UpdateTasksService();
  const task = await updateTaskService.execute({
    id,
    userId: request.user.id,
    ...request.body,
  });

  return response.json(task);
};
