import { Request, Response } from "express";
import DeleteAllTasksService from "../../services/Task/DeleteAllTasksService";

export const deleteAll = async (request: Request, response: Response) => {
    const deleteAllTasksService = new DeleteAllTasksService();
    await deleteAllTasksService.execute({
        userId: request.user.id,
        params: request.query,
    });
  
    return response.status(204).json();
};