import { Request, Response } from "express";
import DeleteTaskService from "../../services/Task/DeleteTaskService";

export const destroy = async (request: Request, response: Response) => {
    const { id } = request.params;

    const deleteTaskService = new DeleteTaskService();
    await deleteTaskService.execute({
        id,
        userId: request.user.id,
    });
  
    return response.status(204).json();
};
