import { Request, Response } from "express";
import RetrieveTaskService from "../../services/Task/RetrieveTaskService";

export const retrieve = async (request: Request, response: Response) => {
    const { id } = request.params;

    const retrieveTaskService = new RetrieveTaskService();
    const task = await retrieveTaskService.execute({
        userId: request.user.id,
        id,
    });
  
    return response.json(task);
};