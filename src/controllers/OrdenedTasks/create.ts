import { instanceToInstance } from "class-transformer";
import { Request, Response } from "express";
import CreateOrdenedTasksService from "../../services/OrdenedTasks/CreateOrdenedTasks";

export const create = async (request: Request, response: Response) => {
    const { ordenedTasks } = request.body;

    const createOrdenedTasksService = new CreateOrdenedTasksService();
    const ordened = await createOrdenedTasksService.execute({
        userId: request.user.id,
        ordenedTasks,
    });
  
    return response.status(201).json(instanceToInstance(ordened));
};