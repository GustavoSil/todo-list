import express, { Request, Response, NextFunction } from "express";
import "express-async-errors";
import "reflect-metadata";
import connection from "./database";
import routes from "./routes";
import AppError from "./errors/AppError";
import cors from "cors";

connection();

const app = express();
app.use(express.json());

const corsOptions = {
  origin: "*",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  preflightContinue: false,
  optionsSuccessStatus: 204
};

app.use(cors(corsOptions));

app.use(routes);

app.use((err: Error, request: Request, response: Response, _: NextFunction) => {
    if (err instanceof AppError) {
      return response.status(err.statusCode).json({
        message: err.message,
      });
    };

    console.error(err);
  
    return response.status(500).json({
      status: "error",
      message: "Internal server error",
    });
});

export default app;
