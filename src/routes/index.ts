import { Router } from "express";
import ordenedTasksRouter from "./ordenedTasks.routes";
import sessionRouter from "./session.routes";
import taskRouter from "./task.routes";
import userRouter from "./user.routes";

const routes = Router();

routes.use("/users", userRouter);
routes.use("/login", sessionRouter);
routes.use("/tasks", taskRouter);
routes.use("/ordened_tasks", ordenedTasksRouter);

export default routes;