import { Router } from "express";
import { create } from "../controllers/OrdenedTasks/create";
import userAuth from "../middlewares/authentication/userAuth";
import { schemaValidate } from "../middlewares/validate/schemaValidate";
import { createOrdenedTasksSchema } from "../models/schemas/OrdenedTasks/createOrdenedTasks";

const ordenedTasksRouter = Router();

ordenedTasksRouter.post("/", schemaValidate(createOrdenedTasksSchema), userAuth, create);

export default ordenedTasksRouter;