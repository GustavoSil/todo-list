import { Router } from "express";
import { create } from "../controllers/Task/create";
import { deleteAll } from "../controllers/Task/deleteAll";
import { destroy } from "../controllers/Task/destroy";
import { list } from "../controllers/Task/list";
import { retrieve } from "../controllers/Task/retrieve";
import { update } from "../controllers/Task/update";
import userAuth from "../middlewares/authentication/userAuth";
import { schemaValidate } from "../middlewares/validate/schemaValidate";
import { createTaskSchema } from "../models/schemas/Task/createTask";
import { updateTaskSchema } from "../models/schemas/Task/updateTask";

const taskRouter = Router();

taskRouter.post("/", userAuth, schemaValidate(createTaskSchema), create);
taskRouter.get("/", userAuth, list);
taskRouter.get("/:id", userAuth, retrieve);
taskRouter.delete("/delete_all", userAuth, deleteAll);
taskRouter.patch("/:id", userAuth, schemaValidate(updateTaskSchema), update);
taskRouter.delete("/:id", userAuth, destroy);

export default taskRouter;