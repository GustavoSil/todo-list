import { Router } from "express";
import { login } from "../controllers/Session/login";

const sessionRouter = Router();

sessionRouter.post("/", login);

export default sessionRouter;