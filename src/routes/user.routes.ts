import { Router } from "express";
import { create } from "../controllers/User/create";
import { schemaValidate } from "../middlewares/validate/schemaValidate";
import { createUserSchema } from "../models/schemas/User/createUser";

const userRouter = Router();

userRouter.post("/", schemaValidate(createUserSchema), create);

export default userRouter;