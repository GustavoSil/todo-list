import { Exclude } from "class-transformer";
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm";
import Task from "./Task";
import User from "./User";

@Entity("ordened_tasks")
class OrdenedTask {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ManyToOne(() => User)
    user: User;

    @ManyToOne(() => Task, {eager: true})
    task: Task;

    @Exclude()
    @Column()
    userId: string;  

    @Exclude()
    @Column()
    taskId: string;

    @Column()
    order: number;

    @CreateDateColumn()
    createdOn: Date;

    @UpdateDateColumn()
    updatedOn: Date;
}

export default OrdenedTask;