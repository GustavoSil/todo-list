import { Request, Response, NextFunction } from "express";
import { verify } from "jsonwebtoken";
import { getRepository } from "typeorm";
import User from "../../entities/User";
import AppError from "../../errors/AppError";

interface TokenPayload {
  iat: number;
  exp: number;
  sub: string;
}

export default async function userAuth(
  request: Request,
  response: Response,
  next: NextFunction
): Promise<void> {
    const authHeader = request.headers.authorization;

    if (!authHeader) {
        throw new AppError("JWT is missing", 401);
    };

    const [, token] = authHeader.split(" ");
    const { JWT_SECRET } = process.env;

    if (!JWT_SECRET) throw new Error("JWT secret missing.");

    try {
        const decoded = verify(token, JWT_SECRET);
        const { sub } = decoded as TokenPayload;

        const userRepository = getRepository(User);
        const user = await userRepository.findOne({
            where: {
                id: sub,
            },
        });
    
        if (!user) throw new AppError("JWT Expired or sended in a wrong way");

        request.user = {
            id: sub,
        };
    } catch (e) {
        throw new AppError("JWT Expired or sended in a wrong way");
    };

    return next();
}
