import { getRepository } from "typeorm";
import AppError from "../../errors/AppError";
import { compare } from "bcryptjs";
import { sign } from "jsonwebtoken";
import User from "../../entities/User";

interface Request {
  email: string;
  password: string;
}

interface Response {
  token: string;
}

export default class AuthService {
    public async execute({ email, password }: Request): Promise<Response> {
        const userRepository = getRepository(User);

        const user = await userRepository.findOne({
            where: {
                email,
            }
        });

        if (!user) throw new AppError("Wrong email/password", 401);

        const passwordMatch = await compare(password, user.password);

        if (!passwordMatch) throw new AppError("Wrong email/password", 401);

        const { JWT_EXPIRES_IN, JWT_SECRET } = process.env;

        if (!JWT_SECRET) throw new Error("JWT secret missing.");

        const token = sign({}, JWT_SECRET, {
            subject: user.id,
            expiresIn: JWT_EXPIRES_IN,
        });

        return {
            token,
        };
    }
};
