import { getRepository } from "typeorm";
import OrdenedTask from "../../entities/OrdenedTask";
import AppError from "../../errors/AppError";
import RetrieveTaskService from "../Task/RetrieveTaskService";

interface IOrdenedTasks {
    taskId: string;
    order: number;
}

interface Request {
    userId: string;
    ordenedTasks: IOrdenedTasks[];
};

export default class CreateOrdenedTasksService {
    public async execute({ userId, ordenedTasks }: Request): Promise<OrdenedTask[]> {
        const ordenedTasksRepository = getRepository(OrdenedTask);

        const ordenedTasksFind = await ordenedTasksRepository.find({
            where: {
                userId,
            }
        });

        if (ordenedTasksFind) {
            for (let i = 0; i < ordenedTasksFind.length; i++) {
                await ordenedTasksRepository.delete({id: ordenedTasksFind[i].id})
            };
        };

        let ordened = [];
        for (let i = 0; i < ordenedTasks.length; i++) {
            const retrieveTaskService = new RetrieveTaskService();
            await retrieveTaskService.execute({
                userId,
                id: ordenedTasks[i].taskId,
            }).catch(e => { throw new AppError("Task not found.", 404) });
            
            const ordenedTask = ordenedTasksRepository.create({
                user: {
                    id: userId
                },
                task: {
                    id: ordenedTasks[i].taskId
                },
                order: ordenedTasks[i].order,
            });
            
            await ordenedTasksRepository.save(ordenedTask);
            ordened.push(ordenedTask);
        }

        return ordened;
    }
};