import { getRepository } from "typeorm";
import OrdenedTask from "../../entities/OrdenedTask";
import Task from "../../entities/Task";
import CreateOrdenedTasksService from "../OrdenedTasks/CreateOrdenedTasks";

interface Request {
    userId: string;
    description: string;
};

export default class CreateTaskService {
    public async execute({ description, userId }: Request): Promise<Task> {
        const taskRepository = getRepository(Task);
        const task = taskRepository.create({
            user: {
                id: userId
            },
            description,
        });

        await taskRepository.save(task);

        const ordenedTasksRepository = getRepository(OrdenedTask);
        const ordenedTasks = await ordenedTasksRepository
            .createQueryBuilder("ordened_tasks")
            .where('ordened_tasks.userId = :userId', { userId })
            .orderBy('ordened_tasks.order')
            .getMany();
        
        ordenedTasksRepository.delete({userId})
        
        if (ordenedTasks.length > 0) {
            const ordenedTask = ordenedTasksRepository.create({
                user: {
                    id: userId
                },
                task: {
                    id: task.id
                },
                order: ordenedTasks[ordenedTasks.length - 1].order + 1
            })
            
            ordenedTasks.push(ordenedTask);

            const createOrdenedTasksService = new CreateOrdenedTasksService();
            await createOrdenedTasksService.execute({
                userId,
                ordenedTasks,
            });
        }
        
        return task;
    }
};
