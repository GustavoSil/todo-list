import { getRepository } from "typeorm";
import Task from "../../entities/Task";
import AppError from "../../errors/AppError";

interface Request {
    userId: string;
    params: object;
};

export default class DeleteAllTasksService {
    public async execute({ userId, params }: Request): Promise<void> {
        const taskRepository = getRepository(Task);
        const tasks = await taskRepository.find({
            where: {
                userId,
                ...params,
            }
        })

        if (!tasks) throw new AppError("Tasks not found.", 404);

        for (let i = 0; i < tasks.length; i++) {
            await taskRepository.delete({ id: tasks[i].id });
        };
    }
};