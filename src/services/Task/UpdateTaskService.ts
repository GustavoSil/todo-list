import { getRepository } from "typeorm";
import Task from "../../entities/Task";
import AppError from "../../errors/AppError";

interface Request {
    id: string
    userId: string;
    description?: string;
    status?: string;
};

export default class UpdateTasksService {
    public async execute({ id, description, status, userId }: Request): Promise<Task> {
        const taskRepository = getRepository(Task);
        const task = await taskRepository.findOne({
            where: {
                id,
                userId
            }
        });

        if (!task) throw new AppError("Task not found.", 404);

        description ? (task.description = description) : task.description;
        status ? (task.status = status) : task.status;

        try {
            await taskRepository.save(task);
        } catch (e) {
            throw new AppError("error updating task.")
        };

        return task;
    }
};
