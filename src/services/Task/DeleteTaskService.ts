import { DeleteResult, getRepository } from "typeorm";
import Task from "../../entities/Task";
import AppError from "../../errors/AppError";

interface Request {
    id: string;
    userId: string;
};

export default class DeleteTaskService {
    public async execute({ id, userId }: Request): Promise<DeleteResult> {
        const taskRepository = getRepository(Task);
        const task = await taskRepository.findOne({
            where: {
                id,
                userId,
            }
        })

        if (!task) throw new AppError("Task not found.", 404);
        
        return taskRepository.delete(id);
    }
};
