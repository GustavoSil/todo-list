import { getRepository } from "typeorm";
import OrdenedTask from "../../entities/OrdenedTask";
import Task from "../../entities/Task";

interface Request {
    userId: string;
    params: object;
};

export default class ListTasksService {
    public async execute({ userId, params }: Request) {
        const ordenedTasksRepository = getRepository(OrdenedTask);
        const ordenedTasks = await ordenedTasksRepository
            .createQueryBuilder("ordened_tasks")
            .where('ordened_tasks.userId = :userId', { userId })
            .orderBy('ordened_tasks.order')
            .getMany();

        if (ordenedTasks.length > 0) {
            const taskRepository = getRepository(Task);
            const tasks = [];
            for (let i = 0; i < ordenedTasks.length; i++) {
                const task = await taskRepository.findOne({
                    where: {
                        id: ordenedTasks[i].taskId,
                        ...params,
                    }
                })
                    
                if (task) tasks.push(task);
            }

            return tasks;
        }

        const taskRepository = getRepository(Task);
        const tasks = await taskRepository.find({
            where: {
                userId,
                ...params,
            }
        });
        
        return tasks;
    }
};
