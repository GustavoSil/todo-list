import { getRepository } from "typeorm";
import Task from "../../entities/Task";
import AppError from "../../errors/AppError";

interface Request {
    userId: string;
    id: string;
};

export default class RetrieveTaskService {
    public async execute({ userId, id }: Request): Promise<Task> {
        const taskRepository = getRepository(Task);
        const task = await taskRepository.findOne({
            where: {
                id,
                userId,
            }
        })

        if (!task) throw new AppError("Task not found.", 404);

        return task;
    }
};